#include<stdio.h>
float input();
float area(float);
void output(float);
void main()
{
    float radius;
    float ar;
    radius=input();
    ar=area(radius);
    output(ar);
}
float input()
{
    float r ;
    printf("enter the radius of the circle ");
    scanf("%f",r);
    return r;
}
float area(float r)
{
    float a;
    a=3.14*r*r;
    return a;
}
void output(float a)
{
    printf("area of the circle with the given radius= %f",a);
}